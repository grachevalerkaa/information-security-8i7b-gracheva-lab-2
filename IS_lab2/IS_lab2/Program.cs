﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IS_lab2
{
    class Program
    {
        static char[] alphabet = new char[72]
        {
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з',
            'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р',
            'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ',
            'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'А', 'Б', 'В',
            'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К',
            'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
            'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь',
            'Э', 'Ю', 'Я', ' ', '.', ':', '!', '?', ','
        };

        static string inputPhrase;
        static string key;
        static string encryptedPhrase;
        static string decryptedPhrase;

        static void Main(string[] args)
        {
            while(true)
            {
                while(true)
                {
                    Console.WriteLine("Введите фразу, которую необходимо зашифровать:");
                    inputPhrase = Console.ReadLine();
                    if (!FindInappropriateCharacter(inputPhrase) && inputPhrase.Length > 0)
                        break;
                    else Console.WriteLine("Фраза не удовлетворяет критериям алфавита!");
                }
                while(true)
                {
                    Console.WriteLine("Введите ключ:");
                    key = Console.ReadLine();
                    if (!FindInappropriateCharacter(key) && key.Length > 0)
                        break;
                    else
                        Console.WriteLine("Введен неверный ключ!");
                }

                EncryptPhrase();
                Console.WriteLine("Зашифрованная последовательность:");
                Console.WriteLine(string.Join("", encryptedPhrase));

                DecryptPhrase();
                Console.WriteLine("Дешифрированная последовательность:");
                Console.WriteLine(decryptedPhrase);
                
                Console.WriteLine();
            }
        }

        static bool FindInappropriateCharacter(string input)
        {
            for (int elemNumber = 0; elemNumber < input.Length; elemNumber++)
            {
                if (!alphabet.Contains(input[elemNumber]))
                {
                    return true;
                }
            }
            return false;
        }

        static void EncryptPhrase()
        {
            int x = -1;
            int k = -1;
            encryptedPhrase = "";
            for (int elemNumber = 0; elemNumber < inputPhrase.Length; elemNumber++)
            {
                x = Array.IndexOf(alphabet, inputPhrase[elemNumber]);
                k = Array.IndexOf(alphabet, key[elemNumber % (key.Length)]);
                encryptedPhrase += alphabet[(x + k) % alphabet.Length];
            }
        }

        static void DecryptPhrase()
        {
            int elemAlphabetIndex;
            int k = - 1;
            int x = - 1;
            decryptedPhrase = "";
            for (int elemNumber = 0; elemNumber < encryptedPhrase.Count(); elemNumber++)
            {
                elemAlphabetIndex = Array.IndexOf(alphabet, encryptedPhrase[elemNumber]);
                k = Array.IndexOf(alphabet, key[elemNumber % (key.Length)]);
                x = elemAlphabetIndex >= k ? elemAlphabetIndex - k : elemAlphabetIndex - k + alphabet.Length;
                decryptedPhrase += alphabet[x];
            }
        }
    }
}
